#!/usr/bin/env bash

# Copyright (C): copy it in the right way!
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# This file is a modified version of the SimpleGapps make script by ezio84 and NYCHitman1.

# Usage:
# $ (source|bash|.) sGapps.sh

# Define paths & variables
TARGETDIR=$(pwd)
BASE="$TARGETDIR"/base
SMALL="$TARGETDIR"/small
TOOLSDIR="$TARGETDIR"/tools
STAGINGDIR="$TARGETDIR"/staging
FINALDIR="$TARGETDIR"/out
DATE=$(date '+%Y%m%d')
ZIPNAME=sGapps-small-arm-10-"$DATE".zip
JAVAHEAP=3072m
SIGNAPK="$TOOLSDIR"/signapk.jar
MINSIGNAPK="$TOOLSDIR"/minsignapk.jar
TESTKEYPEM="$TOOLSDIR"/testkey.x509.pem
TESTKEYPK8="$TOOLSDIR"/testkey.pk8

# Colors
green=`tput setaf 2`
red=`tput setaf 1`
yellow=`tput setaf 3`
reset=`tput sgr0`

# Decompression function for apks
dcapk() {
  TARGETDIR=$(pwd)
  TARGETAPK="$TARGETDIR"/$(basename "$TARGETDIR").apk
  unzip -qo "$TARGETAPK" -d "$TARGETDIR" "lib/*"
  zip -qd "$TARGETAPK" "lib/*"
  cd "$TARGETDIR"
  zip -qrDZ store -b "$TARGETDIR" "$TARGETAPK" "lib/"
  rm -rf "${TARGETDIR:?}"/lib/
  mv -f "$TARGETAPK" "$TARGETAPK".orig
  zipalign -fp 4 "$TARGETAPK".orig "$TARGETAPK"
  rm -f "$TARGETAPK".orig
}

echo ""; echo "Making sGapps."; echo ""

BEGIN=$(date +%s)
export PATH="$TOOLSDIR":$PATH
cp -rf "$BASE"/* "$STAGINGDIR"
cp -rf "$SMALL"/* "$STAGINGDIR"

cd "$STAGINGDIR/${dirs}";
dcapk 1> /dev/null 2>&1;
   
cd "$STAGINGDIR"
zip -qr9 "$ZIPNAME" ./* -x "placeholder"
java -Xmx"$JAVAHEAP" -jar "$SIGNAPK" -w "$TESTKEYPEM" "$TESTKEYPK8" "$ZIPNAME" "$ZIPNAME".signed
rm -f "$ZIPNAME"
zipadjust "$ZIPNAME".signed "$ZIPNAME".fixed 1> /dev/null 2>&1
rm -f "$ZIPNAME".signed
java -Xmx"$JAVAHEAP" -jar "$MINSIGNAPK" "$TESTKEYPEM" "$TESTKEYPK8" "$ZIPNAME".fixed "$ZIPNAME"
rm -f "$ZIPNAME".fixed
mkdir --parents "$FINALDIR"
md5sum "$ZIPNAME" > "$FINALDIR"/"$ZIPNAME".md5sum
mv -f "$ZIPNAME" "$FINALDIR"/"$ZIPNAME"
ls | grep -iv "placeholder" | xargs rm -rf
cd ../

if [[ -f ${FINALDIR}/${ZIPNAME} ]]; then
	END=$(date +%s)
    echo "${green}sGapps Complete!!${reset}"; echo ""
    echo "${green}Total time elapsed: $( echo $(( ${END}-${BEGIN} )) | awk '{print int($1/60)"mins "int($1%60)"secs "}' )${reset}"
    echo "${green}Zip location: ${FINALDIR}${reset}"
    echo "${green}Zip size: $( du -h ${FINALDIR}/${ZIPNAME} | awk '{print $1}' )${reset}"
    echo ""
else
	END=$(date +%s)
    echo "${red}sGapps compilation failed!${reset}"; echo ""
    echo "${yellow}Total time elapsed: $( echo $(( ${END}-${BEGIN} )) | awk '{print int($1/60)"mins "int($1%60)"secs "}' )${reset}"
    echo ""
fi
