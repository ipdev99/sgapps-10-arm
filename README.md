## sGapps for Android 10
### arm
-- Still in Testing --  

**Mini**  
_Doesn't remove custom rom built-in stuff_  
- Core Google
- Some extra stuff..  

**Small**  
~~_Replaces keyboard with Google keyboard_~~  
- Core Google and keyboard
- Some extra stuff..  

### Recent changes
- Updated some apps  
- Added md5sum to build scripts  
- Some minor cleanup to build scripts  

### Requirements to build
- Linux system with an Android build system setup	
- OpenJDK 8

### How to build
Run _git clone https://gitlab.com/ipdev99/sgapps-10-arm.git_  
Run _mksmall.sh_ or _mkmini.sh_ to build  
Flashable zip file will be in the out directory  

### Notes
- Based on Banks, Beans, SimpleGapps and OpenGapps.  
- Feel free to use, change, improve, adapt these gapps. Just remember to share them!  

### Download Links
- _Android File Host_. [Link](https://www.androidfilehost.com/?w=files&flid=303913)
- _Google Drive_. [Link](https://drive.google.com/open?id=1sOUfel3Ykfd-GauAKf3xkaSCFJO3g-j3)
- _MediaFire_. [Link](http://www.mediafire.com/folder/nfaxbp1xsv0qo/arm)
- _SourceForge_. [Link](https://sourceforge.net/projects/sgapps/files/)

### Credits
- Opengapps, Banks, Beans, Surge, Nathan, Ezio, Josh, Edwin, Alex, .MoHaMaD and the Android Community  

